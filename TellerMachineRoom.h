#ifndef TELLERMM_H
#define TELLERMM_H

#include <pthread.h>
#include "06Fifo.h"

typedef struct s_TellerMM
{
  int nclientsInside;
  FIFO fifo;
  pthread_mutex_t mtx;
  pthread_cond_t cWaitingRoomFull;
}TellerMM,*PTellerMM;

void initTellerMachineRoom(PTellerMM pTellerMM);
void useTellerMachine(PTellerMM pTellerMM);
void destroyTellerMachineRoom(PTellerMM pTellerMM);
#endif