#include "TellerMachineRoom.h"
#include <stdio.h>

#define N_USERS 10

void* clientFunct(void*);

int main()
{
  pthread_t idUsers[N_USERS];
  char str[30];
  int i;
  TellerMM tellerMM;

  initTellerMachineRoom(&tellerMM);
  
  for(i=0;i<N_USERS;i++) pthread_create(&idUsers[i],NULL,clientFunct,(void*)&tellerMM);
  
  for(i=0;i<N_USERS;i++) pthread_join(idUsers[i],NULL);
  destroyTellerMachineRoom(&tellerMM);
  printf("type \"Return\" to finish\n");
  fgets(str,sizeof(str),stdin); 
  return 0;
}

void* clientFunct(void* arg)
{
  PTellerMM pTellerMM=(PTellerMM)arg;
  useTellerMachine(pTellerMM);

  return NULL;
}
