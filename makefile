.PHONY: clean all cleanAll install rebuild

all: exe

main.o: main.c
	gcc -Wall -lpthread -c main.c

06Fifo.o: 06Fifo.c 
	gcc -Wall -lpthread -c 06Fifo.c

TellerMachineRoom.o: TellerMachineRoom.c 
	gcc -Wall -lpthread -c TellerMachineRoom.c

06Fifo.o main.o TellerMachineRoom.o: 06Fifo.h TellerMachineRoom.h

exe: 06Fifo.o main.o TellerMachineRoom.o
	gcc 06Fifo.o main.o TellerMachineRoom.o -lpthread -o exe

clean:
	rm -f 06Fifo.o main.o TellerMachineRoom.o

cleanAll: clean
	rm -f exe

rebuild: cleanAll all

install: exe
	cp exe ../id
