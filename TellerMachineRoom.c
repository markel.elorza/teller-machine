#include "TellerMachineRoom.h"


#include <stdio.h>

#define MAX_PEOPLE_INSIDE 6
#define REOPEN 5

void initTellerMachineRoom(PTellerMM pTellerMM) {
    pTellerMM->nclientsInside = 0;
    pthread_mutex_init(&pTellerMM->mtx, NULL);
    pthread_cond_init(&pTellerMM->cWaitingRoomFull,NULL);
    initFifo(&pTellerMM->fifo);
}

void useTellerMachine(PTellerMM pTellerMM) {
    float ticket;
    pthread_mutex_lock(&pTellerMM->mtx);
    while (pTellerMM->nclientsInside == MAX_PEOPLE_INSIDE) pthread_cond_wait(&pTellerMM->cWaitingRoomFull, &pTellerMM->mtx);
    get(&pTellerMM->fifo, &ticket);
    printf("\tUsing machine %f\n", ticket);
    if (ticket > 1)
    {
        sleep(8);
    } else  sleep(3);
    printf("\t\tReleasing machine %f\n", ticket);
    put(&pTellerMM->fifo, ticket);
    pthread_cond_signal(&pTellerMM->cWaitingRoomFull);
    pthread_mutex_unlock(&pTellerMM->mtx);
    return 0;
}

void destroyTellerMachineRoom(PTellerMM pTellerMM) {
    pthread_cond_destroy(&pTellerMM->cWaitingRoomFull);
    pthread_mutex_destroy(&pTellerMM->mtx);
    destroyFifo(&pTellerMM->fifo);
}